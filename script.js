function fetchProducts(limit, sort) {
  if (limit) {
    if (isNaN(limit) || limit < 0) {
      let limitInput = document.getElementsByClassName("limit")[0];
      let paragraph = document.createElement("p");
      paragraph.setAttribute("class", "error-message");
      paragraph.setAttribute("id", "limit-error");
      paragraph.setAttribute("style", "font-size:0.8rem;");
      paragraph.textContent = "Please enter postive number";
      limitInput.insertAdjacentElement("afterend", paragraph);
      return;
    }
  }

  let limitError = document.getElementById("limit-error");
  if (limitError) {
    limitError.remove();
  }

  let productsId = document.getElementById("products");
  productsId.innerHTML = `<div class="loader">
    <div class="lds-dual-ring"></div>
  </div>`;

  let url = "https://fakestoreapi.com/products/";

  let check = document.querySelector('input[name="category"]:checked');

  if (check) {
    if (check.value !== "on") {
      url = `https://fakestoreapi.com/products/category/${check.value.replace(" ", "%20")}`;
    }

    if (sort) {
      url += `?sort=${sort}`;
    }

    if (limit) {
      url += `?limit=${limit}`;
    }
  } else if (limit && sort) {
    url = `https://fakestoreapi.com/products?limit=${limit}&sort=${sort}`;
  } else if (sort) {
    url = `https://fakestoreapi.com/products?sort=${sort}`;
  } else if (limit) {
    url = `https://fakestoreapi.com/products?limit=${limit}`;
  }

  fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(response.status);
      }
    })
    .then((products) => {
      if (products.length === 0) {
        productsId.innerHTML = "";
        let message = document.createElement("p");
        message.setAttribute("class", "no-product");
        message.textContent = "OOPS!! No Products found";
        productsId.appendChild(message);
      } else {
        let productsByCategory = products.reduce((acc, product) => {
          let html = addHtmlForCard(product.id, product.image, product.title, product.price, product.description, product.rating);

          if (acc.hasOwnProperty(product.category)) {
            acc[product.category].push(html);
          } else {
            acc[product.category] = [html];
          }

          return acc;
        }, {});

        productsId.innerHTML = "";

        for (let category in productsByCategory) {
          let heading = document.createElement("h1");
          heading.setAttribute("class", "products-title");
          heading.textContent = category;
          productsId.appendChild(heading);

          let section = document.createElement("section");
          section.setAttribute("class", "products-category");
          section.innerHTML = productsByCategory[category].join("");
          productsId.appendChild(section);
        }
      }
    })
    .catch((err) => {
      let productsId = document.getElementById("products");
      productsId.innerHTML = `<p style="padding:1rem;text-align:center">An Error Occured while fetching the products details. Please check the log for errors</p>`;
      console.error(err);
    });
}

function addHtmlForCard(id, image, title, price, description, rating) {
  let card = `<div class="cards" onclick="showProduct(${id})">
    <img class="cards-image" src="${image}" >
    <div class="cards-text">
    <div class="cards-title-outer">
    <p class="cards-title" >${title}</p>
    </div>
    <p class="cards-description">${description}</p>
    <div class="cards-info">
      <div>
      <p class="price-title">Price:</p>
      <p class="price">$${price}</p>
      </div>
      <div class="rating-box">
      <img src="/images/star.svg" class="cards-rating" >
      <p class="rating">${rating.rate} (${rating.count})</p>
      </div>
      </div>
    </div>
  </div>`;

  return card;
}

fetchProducts();

function showProduct(id) {
  const products = document.getElementById("products-all");
  const productbg = document.getElementById("product-bg");

  products.style.display = "none";
  productbg.style.display = "block";

  productbg.innerHTML = `<div class="loader">
  <div class="lds-dual-ring"></div>
</div>`;

  fetchProduct(id)
    .then((product) => {
      let innerHTML = `
    <section id="product">
    <img class="product-image" src="${product.image}" />
    <div class="product-info">
      <p class="product-category">${product.category}</p>
      <h2>${product.title}</h2>
      <div class="rating-box">
        <img src="/images/star.svg" class="cards-rating" />
        <p class="rating">${product.rating.rate} (${product.rating.count})</p>
      </div>
      <p class="product-price">$${product.price}</p>
      <p class="product-description">
        ${product.description}
      </p>
    </div>
  </section>`;
      productbg.innerHTML = innerHTML;
      window.history.pushState({ pageTitle: "Single product" }, "", `/product/${id}`);
    })
    .catch((err) => {
      productbg.innerHTML = `<p style="padding:1rem;text-align:center">An Error Occured while fetching the product details. Please check the log for errors</p>`;
      console.error(err);
    });
}

window.onpopstate = function () {
  if (location.pathname === "/" || location.pathname === "/index.html") {
    const products = document.getElementById("products-all");
    const product = document.getElementById("product-bg");

    products.style.display = "flex";
    product.style.display = "none";
  } else {
    const products = document.getElementById("products-all");
    const product = document.getElementById("product-bg");

    products.style.display = "none";
    product.style.display = "block !important";
  }
};

function fetchProduct(productId) {
  return fetch(`https://fakestoreapi.com/products/${productId}`).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error(response.status);
    }
  });
}

function fetchCategories() {
  let categories = document.getElementById("categories");

  fetch("https://fakestoreapi.com/products/categories")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let allCategories = data.reduce((acc, category) => {
        acc += `<div><input class="radio-btn" type="radio" id="${category.replace(
          " ",
          "%20"
        )}" name="category" value="${category}" onclick=fetchProducts(null,null,"${category.replace(" ", "%20")}") /><label for="${category.replace(
          " ",
          "%20"
        )}"> ${category}<label></div>`;

        return acc;
      }, `<div><input type="radio" class="radio-btn" id="all" name='category' onclick=fetchProducts(null,null,all) checked /><label for="all">All Categories</label></div>`);

      categories.innerHTML = allCategories;
    });
}

fetchCategories();
