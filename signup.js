function validateSignup(event) {
  event.preventDefault();
  const firstName = document.getElementById("first-name");
  const lastName = document.getElementById("last-name");
  const email = document.getElementById("email");
  const password = document.getElementById("password");
  const repeatPassword = document.getElementById("repeat-password");
  const tos = document.getElementById("agree-tos");

  let errorOccured = false;
  removeAllErrorMessages();
  removeErrorBorder([firstName, lastName, email, password, repeatPassword]);

  if (firstName.value.trim() == "" || !/^[a-zA-Z]+$/.test(firstName.value)) {
    displayErrorMessage(firstName, "First Name is required and should contain only alphabets");
    errorOccured = true;
  } else {
    makeFieldGreen(firstName);
  }

  if (lastName.value.trim() == "" || !/^[a-zA-Z]+$/.test(lastName.value)) {
    displayErrorMessage(lastName, "Last Name is required and should contain only alphabets");
    errorOccured = true;
  } else {
    makeFieldGreen(lastName);
  }

  if (email.value == "") {
    displayErrorMessage(email, "Email is required");
    errorOccured = true;
  } else if (
    !/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email.value)
  ) {
    errorOccured = true;

    displayErrorMessage(email, "Enter valid email");
  } else {
    makeFieldGreen(email);
  }

  if (password.value == "" || password.value.length < 8 || !/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password.value)) {
    displayErrorMessage(
      password,
      "Password should contain at least 1 lower alphabet, 1 upper alphabet, 1 special character, and 8 characters in length"
    );
  } else {
    makeFieldGreen(password);
  }

  if (password.value !== repeatPassword.value) {
    displayErrorMessage(repeatPassword, "Password did not match");
    errorOccured = true;
  } else if (repeatPassword.value == "") {
    displayErrorMessage(repeatPassword, "Password is required");
    errorOccured = true;
  } else {
    makeFieldGreen(repeatPassword);
  }

  if (!tos.checked) {
    let agreeTos = document.getElementsByClassName("agree-tos");
    let paragraph = document.createElement("p");
    paragraph.textContent = "Please agree to Terms and Conditions";
    paragraph.setAttribute("class", "error-message");
    agreeTos[0].insertAdjacentElement("afterend", paragraph);
    errorOccured = true;
  }

  if (!errorOccured) {
    let btn = document.getElementById("form-submit-btn");
    let redirect = document.createElement("p");
    redirect.textContent = "You will be redirected in 3 seconds";
    btn.insertAdjacentElement("afterend", redirect);
    let para = document.createElement("p");
    para.setAttribute("style", "color:green;");
    para.textContent = "Sign up Successful";
    btn.insertAdjacentElement("afterend", para);

    setTimeout(() => {
      window.location = "/";
    }, 3000);

    localStorage.setItem("name", firstName.value);
    localStorage.setItem("email", email.value);
  }
}

function displayErrorMessage(element, message) {
  element.style.border = "1px solid red";
  element.setAttribute("class", "removeBottomMargin");
  let errorMessage = document.createElement("p");
  errorMessage.setAttribute("class", "error-message");
  errorMessage.textContent = message;
  element.insertAdjacentElement("afterend", errorMessage);
}

function removeErrorBorder() {
  const firstName = document.getElementById("first-name");
  firstName.removeAttribute("class");
  firstName.style = "";

  const lastName = document.getElementById("last-name");
  lastName.removeAttribute("class");
  lastName.style = "";

  const email = document.getElementById("email");
  email.removeAttribute("class");
  email.style = "";

  const password = document.getElementById("password");
  password.removeAttribute("class");
  password.style = "";

  const repeatPassword = document.getElementById("repeat-password");
  repeatPassword.removeAttribute("class");
  repeatPassword.style = "";
}

function checkIfUserAlreadyLoggedIn() {
  if (localStorage.getItem("name") && localStorage.getItem("email")) {
    let signupContainer = document.getElementsByClassName("signup-container");
    let signupForm = document.getElementById("form");
    signupForm.style.display = "none";

    let paragraph = document.createElement("p");
    paragraph.setAttribute("id", "loggedin");
    paragraph.textContent = `Hi, ${localStorage.getItem("name")}. You are already signed up.`;
    signupContainer[0].insertAdjacentElement("beforeend", paragraph);

    let button = document.createElement("button");
    button.setAttribute("id", "logout-button");
    button.setAttribute("onclick", "logout()");
    button.textContent = "Log Out";
    signupContainer[0].insertAdjacentElement("beforeend", button);
  } else {
    let signupForm = document.getElementById("form");
    signupForm.style.display = "block";

    let loggedin = document.getElementById("loggedin");

    let button = document.getElementById("logout-button");
    if (loggedin && button) {
      button.remove();
      loggedin.remove();
    }
  }
}

checkIfUserAlreadyLoggedIn();

function logout() {
  localStorage.clear();
  checkIfUserAlreadyLoggedIn();
}

function makeFieldGreen(element) {
  element.style.border = "1px solid green";
}

function removeAllErrorMessages() {
  const allErrors = document.getElementsByClassName("error-message");

  while (allErrors.length > 0) {
    allErrors[0].parentNode.removeChild(allErrors[0]);
  }
}
